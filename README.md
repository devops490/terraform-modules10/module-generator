# Terraform module generator

Scaffolding for new Terraform module projects

### Features

- `main.tf`, `variables.tf`,`outputs.tf` files to module root path

- `.editorconfig`, `.gitignore`, `.gitattributes` and `.terraform-version` files to module root path

- `test` directory with an example test based on ```terratest```

- `.pre-commit-config.yaml` for `terraform fmt`, `terraform-docs`, `check-merge-conflict` and (`go fmt`, `golint`) / `rubocop`

- `example` directory with module usage tf files

### Prerequisites

- [terraform](https://learn.hashicorp.com/terraform/getting-started/install#installing-terraform) `pro tip: use tfenv`
- [terraform-docs](https://github.com/segmentio/terraform-docs)
- [pre-commit](https://pre-commit.com/#install)
- For tests
  - [golang](https://golang.org/doc/install#install) `pro tip: use gvm`
  - [golint](https://github.com/golang/lint#installation)

### Installation

- To use generator using Docker, Install [Docker](https://docs.docker.com/engine/install/) `recommended`
- To use generator using Nodejs, Install [nodejs](https://nodejs.org/en/download/) `pro tip: use nvm`

### Usage

To use the included generator execute the below command in shell and provide your answers to the prompts.

```sh
> npm link
> yo tf-generator
```

##### Prompts:
```sh
     _-----_     
    |       |    ╭──────────────────────────╮
    |--(o)--|    │ Welcome to the tf-module │
   `---------´   │        generator!        │
    ( _´U`_ )    ╰──────────────────────────╯
    /___A___\   /
     |  ~  |     
   __'.___.'__   
 ´   `  |° ´ Y ` 
? Enter name for the new terraform module :  example-module
? Enter description for the new terraform module :  Example terraform module
? Enter author name :  sudokar
? Choose terraform version (Use arrow keys)
  0.15
  0.14
❯ 0.13
  0.12
  0.11
```

Project layout generated for the new module

```
example-module
├── .editorconfig
├── .gitattributes
├── .gitignore
├── .pre-commit-config.yaml
├── .terraform-version
├── README.md
├── main.tf
├── outputs.tf
├── variables.tf
├── alarm.tf
├── log.tf
├── role.tf
├── example
│   ├── main.tf
│   ├── outputs.tf
│   └── variables.tf
├── test
    └── example_test.go
```

##### Post generation steps

Step 1

On the generated module's root path, Initialize git repository

```sh
git init
```

Step 2

On the generated module's root path, Install pre-commit hooks

```sh
pre-commit install
```

Step 3

For terratest, get below libs

```sh
> go get github.com/gruntwork-io/terratest/modules/terraform
> go get github.com/stretchr/testify/assert
```
'use strict';
const Generator = require('yeoman-generator');
const yosay = require('yosay');

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);
  }

  async prompting() {
    this.log(
      yosay('Welcome to the tf-module generator!')
    );

    this.answers = await this.prompt([{
      type: 'input',
      name: 'name',
      message: 'Enter name for the new terraform module : ',
    },
    {
      type: 'input',
      name: 'description',
      message: 'Enter description for the new terraform module : ',
    },
    {
      type: 'input',
      name: 'author',
      message: 'Enter author name : ',
    },
    {
      type: 'list',
      name: 'tfVersion',
      message: 'Choose terraform version',
      choices: [
        {
          name: '0.15',
          value: '15',
          checked: true
        },
        {
          name: '0.14',
          value: '14'
        },
        {
          name: '0.13',
          value: '13'
        },
        {
          name: '0.12',
          value: '12',
        },
      ]
    }
    ]);
  }

  writing() {
    this.destinationRoot(this.answers.name);

    this.fs.copyTpl(
      `${this.templatePath()}/.!(gitignorefile|gitattributesfile|pre-commit-config|terraform-version)*`,
      this.destinationRoot(),
      this.props
    );

    this.fs.copyTpl(
      this.templatePath('.gitignorefile'),
      this.destinationPath(`.gitignore`), {
      testFramework: this.answers.testFramework
    }
    );

    // .gitattributes
    this.fs.copyTpl(
      this.templatePath('.gitattributesfile'),
      this.destinationPath(`.gitattributes`), {
      testFramework: this.answers.testFramework
    }
    );

    // .pre-commit-config.yaml
    this.fs.copyTpl(
      this.templatePath('.pre-commit-config.yaml'),
      this.destinationPath(`.pre-commit-config.yaml`), {
      testFramework: this.answers.testFramework
    }
    );

    // .terraform-version
    this.fs.copyTpl(
      this.templatePath('.terraform-version'),
      this.destinationPath(`.terraform-version`), {
      tfVersion: this.answers.tfVersion
    }
    );

    // Template
    this.fs.copyTpl(
      `${this.templatePath()}/**/*.tf`,
      this.destinationRoot()
    );

    // Terratest
    this.fs.copyTpl(
      `${this.templatePath()}/test/terratest/*.go`,
      `${this.destinationRoot()}/test`
    );

    // README
    this.fs.copyTpl(
      this.templatePath('_README.md'),
      this.destinationPath('README.md'), {
      name: this.answers.name,
      description: this.answers.description,
      author: this.answers.author,
      testFramework: this.answers.testFramework
    }
    );
  }
};
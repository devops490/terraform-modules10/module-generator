#!/bin/bash

if ! which jq > /dev/null ; then
  echo "This build script now requires 'jq' to build." 1>&2
  echo "Find it at: https://stedolan.github.io/jq/" 1>&2
  exit 1
fi

OWNER="${OWNER:-soham}"
NAME="${NAME:-$(cat ../package.json | jq -r '.name')}"

VERSION="${VERSION:-$(cat ../package.json | jq -r '.version')}"

BUILD_DATE="${BUILD_DATE:-$(date -u +"%Y-%m-%dT%H:%M:%SZ")}"
VCS_REF="${VCS_REF:-$(git rev-parse --short HEAD)}"
DOCKER_ROOT="${DOCKER_ROOT:-$(git rev-parse --show-toplevel)/docker}"

set -o xtrace

# perform the build using the arguments previously supplied
docker build --build-arg BUILD_DATE=$BUILD_DATE \
             --tag $OWNER/$NAME:$VERSION \
             "${DOCKER_ROOT}"

